﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SDFWindow : EditorWindow {
    [MenuItem("Window/SDFGenerator")]
    static void Init()
    {
        SDFWindow window = (SDFWindow)EditorWindow.GetWindow(typeof(SDFWindow));
        window.Show();
    }

    Texture2D inputTex;
    Texture2D outputTex;
    SDFGenerator generator = new SDFGenerator();
    int size = 128;
    bool darkForeground = false;
    private void OnGUI()
    {
        Rect rect = new Rect(10, 10, position.width - 20, 100);
        inputTex = (Texture2D)EditorGUI.ObjectField(rect, "Input Texture", inputTex, typeof(Texture2D), false);
        
        if(inputTex != null)
        {
            rect.y += rect.height + 10;
            rect.height = EditorGUIUtility.singleLineHeight;
            size = EditorGUI.IntSlider(rect, "Size:", size, 32, 512);
            rect.y += rect.height + 10;
            darkForeground = EditorGUI.Toggle(rect, "Foreground is Dark ？", darkForeground);
            rect.y += rect.height + 10;
            rect.height = 50;
            if (GUI.Button(rect, "Go"))
            {
                outputTex = generator.Generate(inputTex, size, size, darkForeground);
            }
        }
        if(outputTex != null)
        {
            rect.y += rect.height + 10;

            rect.height = position.width - 20;
            EditorGUI.DrawPreviewTexture(rect, outputTex);
            rect.y += rect.height + 10;
            rect.height = 50;
            if (GUI.Button(rect, "Save"))
            {
                var path = EditorUtility.SaveFilePanelInProject("Save SDF Texture", inputTex.name + "_SDF", "png", "", System.IO.Path.GetDirectoryName(AssetDatabase.GetAssetPath(inputTex)));
                if(!string.IsNullOrEmpty(path))
                {
                    System.IO.File.WriteAllBytes(path, outputTex.EncodeToPNG());
                    AssetDatabase.Refresh();
                    var importer = AssetImporter.GetAtPath(path) as TextureImporter;
                    importer.textureType = TextureImporterType.SingleChannel;
                    importer.maxTextureSize = size;
                    importer.mipmapEnabled = false;
                    importer.alphaSource = TextureImporterAlphaSource.None;
                    importer.SaveAndReimport();
                }
            }
        }
    }
}
