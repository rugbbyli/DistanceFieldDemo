﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/SDFTexture"
{
    Properties
    {
        [PreRenderer][HideInInspector]_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
        _SDFTex("SDFTexture", 2D) = "white" {}
        _DistanceMark("Distance Mark", float) = .5
        _SmoothDelta("Smooth Delta", float) = .25
        _MainColor("Main Color", Color) = (1,1,1,1)
        
        _OutlineDelta("Outline Delta", float) = 0.1
        _OutlineColor("Outline Color", Color) = (0,0,0,1)
    }

    SubShader
    {
        Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
        LOD 100

        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
    #pragma vertex vert
    #pragma fragment frag

    #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f {
                float4 vertex : SV_POSITION;
                half2 texcoord : TEXCOORD0;
            };

            sampler2D _MainTex;
            sampler2D _SDFTex;
            float4 _MainTex_ST;
            float4 _MainColor;
            float _SmoothDelta;
            float _DistanceMark;
            float _OutlineDelta;
            float4 _OutlineColor;

            v2f vert(appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.texcoord);
                float distance = tex2D(_SDFTex, i.texcoord).a;

                // do some anti-aliasing
                col.a = 1-smoothstep(_DistanceMark - _SmoothDelta, _DistanceMark + _SmoothDelta, distance);
                //col.a = distance < _DistanceMark ? 1 : 0;
                col.rgb *= _MainColor.rgb;

                //outline
                fixed4 outline = _OutlineColor;
                //outline.a = col.a + _OutlineDelta;
                outline.a = 1-smoothstep(_OutlineDelta - _SmoothDelta, _OutlineDelta + _SmoothDelta, distance);
                
                col = lerp(outline, col, col.a);

                return col;
            }
            ENDCG
        }
    }
}