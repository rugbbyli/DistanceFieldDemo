﻿

using UnityEngine;

public class SDFGenerator
{
    class Grid
    {
        public Vector2[,] grid;
        public int Width, Height;

        public Grid(int width, int height)
        {
            Width = width;
            Height = height;
            grid = new Vector2[height, width];
        }
    }

    Vector2 inside = new Vector2(0, 0);
    Vector2 empty = new Vector2(9999, 9999);

    public Texture2D Generate(Texture2D texture, int height, int width, bool foregroundDark = true)
    {
        var bggs = texture.GetPixel(0, 0).grayscale;
        var throttle = 0.1f;
        System.Func<float, bool> isForeground = (g) => Mathf.Abs(g - bggs) > throttle;
        //System.Func<float, bool> isForeground = (c) => foregroundDark ? c < 0.5f : c > 0.5f;
        var pixels = texture.GetPixels();

        int raw_width = texture.width;
        int raw_height = texture.height;


        var grid1 = new Grid(raw_width, raw_height);
        var grid2 = new Grid(raw_width, raw_height);

        for (int y = 0; y < raw_height; y++)
        {
            for (int x = 0; x < raw_width; x++)
            {
                var g = pixels[y * texture.width + x].grayscale;
                // Points inside get marked with a dx/dy of zero.
                // Points outside get marked with an infinitely large distance.
                if (isForeground(g))
                {
                    Put(grid1, x, y, inside);
                    Put(grid2, x, y, empty);
                }
                else
                {
                    Put(grid2, x, y, inside);
                    Put(grid1, x, y, empty);
                }
            }
        }

        // Generate the SDF.
        GenerateSDF(grid1);
        GenerateSDF(grid2);

        float min_dist = float.MaxValue, max_dist = float.MinValue;
        float[] distances = new float[raw_width * raw_height];
        for (int y = 0; y < raw_height; y++)
        {
            for (int x = 0; x < raw_width; x++)
            {
                // Calculate the actual distance from the dx/dy
                var dist1 = Get(grid1, x, y).magnitude;
                var dist2 = Get(grid2, x, y).magnitude;
                var dist = dist1 - dist2;

                if (min_dist > dist)
                    min_dist = dist;
                if (max_dist < dist)
                    max_dist = dist;

                distances[raw_width * y + x] = dist;
                // Clamp and scale it, just for display purposes.
                //int c = dist * 3 + 128;
                //int c = dist*4 + 128;
                //byte cc = (byte)Mathf.Clamp(c, 0, 255);

                //pixels[width * y + x] = new Color32(cc, cc, cc, cc);
            }
        }
        float dist_len = max_dist - min_dist;
        float scale = 255f / dist_len;
        //Debug.LogFormat("Distance Range:[{0}->{1}], Len:{2}, Zero:{3}", min_dist, max_dist, dist_len, (0f - min_dist) / dist_len);
        
        /*
        d  = [x --- 0  ----------- y]
        nd = [0 --- 0.5 -----------1]
        if(d < 0): nd = (d - x) / -2x = d/-2x + 0.5
        else     : nd = d / 2y + 0.5  = d/+2y + 0.5
         */
        pixels = new Color[raw_width * raw_height];
        for (int i = 0; i < distances.Length; i++)
        {
            float c = Mathf.Clamp01(distances[i] / 85 + 0.5f);
            //float c = Mathf.Clamp01(distances[i] < 0 ? distances[i] / (-2 * min_dist) + 0.5f : distances[i] / (2 * max_dist) + 0.5f);
            pixels[i] = new Color(c, c, c, c);
        }

        var sdf_texture = new Texture2D(raw_width, raw_height, TextureFormat.Alpha8, false);
        sdf_texture.SetPixels(pixels);
        sdf_texture.Apply();
        return sdf_texture;
    }

    Vector2 Get(Grid g, int x, int y)
    {
        // OPTIMIZATION: you can skip the edge check code if you make your grid
        // have a 1-pixel gutter.
        if (x >= 0 && y >= 0 && x < g.Width && y < g.Height)
            return g.grid[y,x];
        else
            return empty;
    }

    void Put(Grid g, int x, int y, Vector2 p)
    {
        g.grid[y,x] = p;
    }

    void Compare(Grid g, ref Vector2 p, int x, int y, int offsetx, int offsety)
    {
        Vector2 other = Get(g, x + offsetx, y + offsety);
        other.x += offsetx;
        other.y += offsety;

        if (other.sqrMagnitude < p.sqrMagnitude)
            p = other;
    }

    void GenerateSDF(Grid g)
    {
        // Pass 0
        for (int y = 0; y < g.Height; y++)
        {
            for (int x = 0; x < g.Width; x++)
            {
                Vector2 p = Get(g, x, y);
                Compare(g, ref p, x, y, -1, 0);
                Compare(g, ref p, x, y, 0, -1);
                Compare(g, ref p, x, y, -1, -1);
                Compare(g, ref p, x, y, 1, -1);
                Put(g, x, y, p);
            }

            for (int x = g.Width - 1; x >= 0; x--)
            {
                Vector2 p = Get(g, x, y);
                Compare(g, ref p, x, y, 1, 0);
                Put(g, x, y, p);
            }
        }

        // Pass 1
        for (int y = g.Height - 1; y >= 0; y--)
        {
            for (int x = g.Width - 1; x >= 0; x--)
            {
                Vector2 p = Get(g, x, y);
                Compare(g, ref p, x, y, 1, 0);
                Compare(g, ref p, x, y, 0, 1);
                Compare(g, ref p, x, y, -1, 1);
                Compare(g, ref p, x, y, 1, 1);
                Put(g, x, y, p);
            }

            for (int x = 0; x < g.Width; x++)
            {
                Vector2 p = Get(g, x, y);
                Compare(g, ref p, x, y, -1, 0);
                Put(g, x, y, p);
            }
        }
    }
}